import os
import shutil
import subprocess
import sys
from pathlib import Path


class Paths:
    project_path = Path(sys.argv[0]).parent.parent
    build_path = project_path / 'build'
    dist_path = project_path / 'dist'
    installer_path = project_path / 'Cumix v1.0.exe'
    standalone_spec_file = 'publish/standalone.spec'
    nsis_nsi = 'publish/install.nsi'


def execute_process(*args):
    process = subprocess.run(
        args,
        cwd=Paths.project_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True
    )
    print(process.stdout)
    return process.returncode


def make_standalone():
    print('Building standalone version', flush=True)
    if execute_process('pyinstaller', Paths.standalone_spec_file) != 0:
        print('Failed to make standalone version')
        exit(1)


def make_installer():
    nsis_path = Path(os.environ['ProgramFiles']) / 'NSIS' / 'makensis.exe'
    if not nsis_path.is_file():
        nsis_path = Path(os.environ['ProgramFiles(x86)']) / 'NSIS' / 'makensis.exe'
    if not nsis_path.is_file():
        print('NSIS not found. Install it from https://nsis.sourceforge.io/Download')
        exit(2)

    print(f'Building installer using {nsis_path}', flush=True)
    if execute_process(str(nsis_path), Paths.nsis_nsi) != 0:
        print('Failed to build installer')
        exit(3)

    execute_process('explorer', '/select, ', str(Paths.installer_path))


def clean_up():
    shutil.rmtree(Paths.build_path, ignore_errors=True)
    print(f'Removed build dir: {Paths.build_path}')

    shutil.rmtree(Paths.dist_path, ignore_errors=True)
    print(f'Removed dist dir: {Paths.dist_path}')


if __name__ == '__main__':
    clean_up()
    make_standalone()
    make_installer()
    clean_up()
