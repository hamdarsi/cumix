import socketserver
import sys
import webbrowser

from app import app
from app.archive_manager import ArchiveManager
from app.controllers import register_templates_folder

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: provide one cbr or cbz comic archive to view')
        exit(1)

    register_templates_folder()
    archive = ArchiveManager.load(sys.argv[1])

    with socketserver.TCPServer(("localhost", 0), None) as s:
        free_port = s.server_address[1]

    webbrowser.open(f'http://127.0.0.1:{free_port}/{archive.escaped_title}')
    app.run(host='0.0.0.0', port=free_port, threaded=True)
