from threading import Thread
from time import sleep

import requests
from flask import request, url_for

import config
from app import app


class WatchDog:
    got_ping = False
    shutdown_url = ''
    started = False

    @staticmethod
    @app.route('/ping')
    def ping():
        WatchDog.got_ping = True

        return ''

    @staticmethod
    @app.route('/shutdown')
    def shutdown():
        request.environ.get('werkzeug.server.shutdown')()
        return ''

    @staticmethod
    def watchdog_func():
        while True:
            WatchDog.got_ping = False
            sleep(config.WATCHDOG_TIMER)
            if not WatchDog.got_ping:
                print('exiting')
                requests.get(WatchDog.shutdown_url)

    @staticmethod
    def start():
        WatchDog.shutdown_url = url_for('shutdown', _external=True)
        if not WatchDog.started:
            Thread(target=WatchDog.watchdog_func).start()
