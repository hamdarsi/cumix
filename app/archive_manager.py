from app.archive import Archive


class ArchiveManager:
    archives = {}

    @staticmethod
    def load(full_path):
        archive = Archive(full_path)
        ArchiveManager.archives[archive.escaped_title] = archive
        return archive

    @staticmethod
    def get(escaped_title):
        return ArchiveManager.archives.get(escaped_title, None)
