import sys
from pathlib import Path

import jinja2
from flask import render_template, make_response, url_for

import config
from app import app
from app.archive_manager import ArchiveManager
from app.watchdog import WatchDog


@app.route('/<escaped_title>')
def get_archive(escaped_title):
    archive = ArchiveManager.get(escaped_title)
    if archive is None:
        return f'Archive "{escaped_title}" is not loaded', 404

    WatchDog.start()
    return render_template(
        'comic.html',
        archive=archive,
        settings=config.Settings,
        watchdog_url=url_for('ping'),
    )


@app.route('/<escaped_title>/<int:page_number>')
def get_archive_page(escaped_title, page_number):
    archive = ArchiveManager.get(escaped_title)
    if archive is None:
        return f'Archive "{escaped_title}" is not loaded', 404

    response = make_response(archive.stream_page(page_number - 1))
    response.headers['Content-Type'] = archive.get_content_type(page_number - 1)
    return response


def register_templates_folder():
    templates_path = Path(sys.argv[0]).parent / 'templates'
    app.jinja_loader = jinja2.ChoiceLoader([
        app.jinja_loader,
        jinja2.FileSystemLoader([templates_path])
        ])
