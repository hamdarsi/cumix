import urllib
from pathlib import Path
from zipfile import ZipFile

import rarfile


def is_picture(item):
    ext = item.lower()[-3:]
    return ext in ('jpg', 'bmp', 'png')


def get_archive_processor(filename):
    ext = filename.lower()[-3:]
    return rarfile.RarFile if ext in ('cbr', 'rar') else ZipFile


class Archive:
    def __init__(self, filename):
        self.filename = Path(filename).stem
        self.processor = get_archive_processor(filename)
        self.archive = self.processor(filename)
        self.escaped_title = urllib.parse.quote_plus(self.filename)
        self.pages = [
            inf
            for inf in self.archive.infolist()
            if is_picture(inf.filename)
        ]

    def page_count(self):
        return len(self.pages)

    def stream_page(self, index: int):
        return self.archive.read(self.pages[index])

    def get_content_type(self, index):
        return self.pages[index].filename.lower()[-3:]
